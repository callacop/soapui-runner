SoapUI Runner is a Spring-based Java web application that can be seen
as a wrapper around SoapUI; SoapUI itself is an open-source
application for testing SOAP and REST-based web services.


Overview
--------

For the purpose of monitoring service uptime, the Land and Resources
Cluster (LRC) of the Government of Ontario uses SoapUI Runner to parse
embedded SoapUI project files (not included in the Github repository)
and execute the SoapUI test cases found against select web services.

Once all test cases have executed, a table is auto-populated and
displayed to the user with the following variables per row:

  1. 'Name'
    - The name of the web service that the test case executed against
  2. 'Success Flag'
    - Boolean value indicating whether the test case passed
  3. 'Log'
    - Link to the error log for the test case if it failed

For the time being, each new SoapUI Runner report is saved to the same
session variable; this means that anytime a new project is run, the
results from the last one will be lost. Database integration via
Hibernate is tentatively planned.


Usage
-----

Before starting SoapUI Runner, first create and place an empty
.properties file named 'soapui-runner.properties' within SoapUI
Runner's WebContent directory. Then, inside the .properties file,
create a key-value pair named 'path' and set its value as the path to
the directory in which you'll place any SoapUI projects that you wish
to make available to SoapUI Runner. Failing to provide a .properties
file as described will generate a null pointer exception upon
application startup.

In regards to service uptime monitoring, each SoapUI test case and web
service should have a one-to-one relationship. This is to ensure that
any error log generated is associated with a single web service.

For each operation within a test case, the following assertions should
be added for checking at the very least:

  1. SOAP Response
    - Checks that the response is a valid SOAP message
  2. Not SOAP Fault
    - Checks that the response is not a SOAP fault


WebSphere Setup
---------------

Each step in the following sections corresponds to individual pages.

Installation:

  1. 'Applications' => 'Application Types' => 'WebSphere enterprise
  applications'
  2. Click the 'Install' button.
  3. Under the 'Local file system' heading, click the 'Browse...'
  button and locate the WAR file to be installed. => 'Next'
  4. 'Fast Path' => 'Next'
  5. "Clusters and servers" => Select the one(s) specified by the
  LRC. => Select the checkbox for the module named 'soapui-runner'. =>
  'Apply' => 'Next'
  6. Select the checkbox for the module named 'soapui-runner'. =>
  Select the virtual host specified by the LRC. => 'Next'
  7. 'Context Root' => Check whether the input is preconfigured to be
  'soapui-runner'. If it is not, type in 'soapui-runner' without the
  single-quotation marks. => 'Next'
  8. 'Next'
  9. 'Finish'
  10. Click the 'Save' link in where it says 'Save directly to the
  master configuration'.

Class Loading and Update Detection:

  1. 'Applications' => 'Application Types' => 'WebSphere enterprise
  applications'
  2. Click the link named 'soapui_runner_v*.*.*_war'.
  3. Under the 'Detail Properties' heading, select 'Class loading and
  update detection'.
  4. Under the 'Class loader order' heading, select the radio button
  next to where it says 'Classes loaded with local class loader
  (parent last)'. => Under the 'WAR class loader policy' heading,
  select the radio button next to where it says 'Single class loader
  for application'. => 'Apply'
  5. Click the 'Save' link in where it says 'Save directly to the
  master configuration'.

Web Server Plug-In Configuration:

  1. 'Environment' => 'Update global Web server plug-in configuration'
  2. 'Ok.'

Startup:

  1. 'Applications' => 'Application Types' => 'WebSphere enterprise
  applications'
  2. Select the checkbox for the module named 'soapui-runner'. =>
  Click the 'Start button'.
  

Version Numbering
-----------------

SoapUI Runner follows the Semantic Versioning standard. For more
information, see the official Semantic Versioning page:

  http://semver.org/


---------------------------------------------------------------------
SoapUI Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2.

SoapUI Runner is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with SoapUI Runner.  If not, see <http://www.gnu.org/licenses/>.
