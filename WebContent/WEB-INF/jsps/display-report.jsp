<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>SoapUI Runner</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="//fonts.googleapis.com/css?family=Raleway:400,300,600"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/normalize.css" />
<link rel="stylesheet" href="css/skeleton.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/html5shiv-printshiv.js"></script>
</head>
<body>
	<div class="container">
		<section class="header">
		<h2 class="title">SoapUI Runner</h2>
		<c:if test="${not empty report.timestamp}">
		Last project ran  -  ${report.name} on ${report.timestamp.getTime()}
		</c:if></section>
		<div class="navbar-spacer"></div>
		<nav class="navbar">
		<div class="container">
			<ul class="navbar-list">
				<li class="navbar-item"><a class="navbar-link"
					href="<c:url value="/" />">Home</a></li>
			</ul>
		</div>
		</nav>
		<div class="docs-section">
			<h6 class="docs-header">Select a SoapUI Project</h6>

			<c:url value="/process-project-selection" var="url" />
			<div class="docs-example docs-example-forms">
				<div class="row">
					<div class="six columns">
						<form:form modelAttribute="project" method="post" action="${url}">
							<label for="projectInput">Directory Listing</label>
							<form:select path="selection" class="u-full-width" >
								<c:forEach items="${directoryListing}" var="file"
									varStatus="loop">
									<form:option value="${file.getPath()}">${file.getName()}</form:option>
								</c:forEach>
							</form:select>
							<a href="<c:url value="/" />" class="button">Refresh Listing</a>&nbsp;<input
								class="button-primary" value="Run" type="submit">
						</form:form>
					</div>
					<div class="six columns"></div>
				</div>
			</div>
		</div>

		<div class="docs-section">
			<h6 class="docs-header">Results</h6>
			<table class="u-full-width">
				<thead>
					<tr>
						<th>Name</th>
						<th>Success Flag</th>
						<th>Log</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${report.resultList}" var="element"
						varStatus="loop">
						<tr>
							<td>${element.name}</td>
							<td>${element.successFlag}</td>
							<td><c:if test="${not empty element.log}">
									<a href="<c:url value="/log/${loop.index}" />" target="_blank">View</a>
								</c:if></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>