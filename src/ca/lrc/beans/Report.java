package ca.lrc.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * The Report program stores the results (each one instantiated as a new Result
 * POJO) of all the test cases run within a SoapUI project.
 * 
 * @author Daniel Vu
 */
@Entity
public class Report implements Serializable {
	private static final long serialVersionUID = 2140144763025238325L;

	@Id
	@GeneratedValue
	private int id;

	@ElementCollection
	private List<Result> resultList = new ArrayList<Result>();

	private String name;
	private Calendar timestamp = Calendar.getInstance();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Result> getResultList() {
		return resultList;
	}

	public void setResultList(List<Result> resultList) {
		this.resultList = resultList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Calendar getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Calendar timestamp) {
		this.timestamp = timestamp;
	}

	public Report() {
	}

	public Report(int id, List<Result> resultList, String name,
			Calendar timestamp) {
		this.id = id;
		this.resultList = resultList;
		this.name = name;
		this.timestamp = timestamp;
	}
}
