package ca.lrc.beans;

import java.io.Serializable;

/**
 * The Project program is a POJO that backs the project selection form in
 * display-report.jsp.
 * 
 * @author Daniel Vu
 */
public class Project implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2438320478166810392L;
	private String selection;

	public String getSelection() {
		return selection;
	}

	public void setSelection(String selection) {
		this.selection = selection;
	}

	public Project() {
	}

	public Project(String selection) {
		this.selection = selection;
	}

}
