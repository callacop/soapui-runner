package ca.lrc.controllers;

import static org.junit.Assert.assertNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

public class GetDirectoryListingTest {
	private static HomeController controller;
	private static File directory;
	private static List<File> directoryListing;

	@BeforeClass
	public static void setUp() throws Exception {
		controller = new HomeController();
		directory = new File("WebContent/soapui-projects");
		directoryListing = new ArrayList<File>();
	}

	@Test(expected = AssertionError.class)
	public void testGetDirectoryFilesReturnsNull() {
		assertNull("Unexpected file(s) exists in provided directory.",
				controller.createDirectoryListing(directory, directoryListing));
	}

	@Test
	public void testGetIntendedFileName() {
		System.out.println(controller
				.createDirectoryListing(directory, directoryListing).get(0)
				.getName());
	}

}
