package ca.lrc.controllers;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.lrc.beans.Report;
import ca.lrc.beans.Result;

import com.eviware.soapui.impl.wsdl.WsdlProject;

public class RunSoapUIProjectTest {
	private static Report expectedReport;
	private static Report actualReport;
	private static HomeController controller;
	private static WsdlProject testProject;

	@BeforeClass
	public static void setUp() throws Exception {
		expectedReport = new Report();
		testProject = new WsdlProject("testServiceUptime-soapui-project.xml");
		controller = new HomeController();
		actualReport = controller.runSoapUIProject(testProject);
	}

	@Test
	public void testExpectedReportMatchesActualReport() {
		boolean flag = true;
		for (int actualResultIndex = 0; actualResultIndex < actualReport
				.getResultList().size(); actualResultIndex++) {
			expectedReport.getResultList().add(
					new Result(actualReport.getResultList()
							.get(actualResultIndex).getName(), true, null));
			boolean successFlagsMatch = (expectedReport.getResultList()
					.get(actualResultIndex).getSuccessFlag() == actualReport
					.getResultList().get(actualResultIndex).getSuccessFlag());
			if (!successFlagsMatch) {
				/*
				 * In case more than one test step fails, a check has been added
				 * to prevent the flag from being set as false more than once
				 */
				if (flag != false) {
					flag = false;
				}
				System.out.println("Expected boolean value of "
						+ expectedReport.getResultList().get(actualResultIndex)
								.getSuccessFlag()
						+ " for "
						+ actualReport.getResultList().get(actualResultIndex)
								.getName()
						+ " and got a boolean value of "
						+ actualReport.getResultList().get(actualResultIndex)
								.getSuccessFlag() + " instead.");
			}
		}
		assertTrue("Expected report does not match actual report.",
				flag == true);
	}

	public void testExpectedReportDoesNotMatchActualReport() {
		boolean flag = true;
		for (int actualResultIndex = 0; actualResultIndex < actualReport
				.getResultList().size(); actualResultIndex++) {
			expectedReport.getResultList().add(
					new Result(actualReport.getResultList()
							.get(actualResultIndex).getName(), true, null));
			boolean successFlagsMatch = (expectedReport.getResultList()
					.get(actualResultIndex).getSuccessFlag() == actualReport
					.getResultList().get(actualResultIndex).getSuccessFlag());

			if (!successFlagsMatch) {
				/*
				 * In case more than one test step fails, a check has been added
				 * to prevent the flag from being set as false more than once
				 */
				if (flag != false) {
					flag = false;
				}
			}
		}
		assertFalse("Expected report matches actual report.", flag == true);
	}

	@Test
	public void testErrorLogsDoNotExist() {
		boolean flag = true;
		for (Result result : actualReport.getResultList()) {
			if (result.getLog() != null) {
				if (flag != false) {
					flag = false;
				}
				System.out.println(result.getName() + " test case failed.");
			}
		}
		assertTrue("Error logs exist.", flag == true);
	}

	public void testErrorLogsDoExist() {
		boolean flag = true;
		for (Result result : actualReport.getResultList()) {
			if (result.getLog() != null) {
				if (flag != false) {
					flag = false;
				}
			}
		}
		assertFalse("Error logs do not exist.", flag == true);
	}
}
