package ca.lrc.controllers;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.xmlbeans.XmlException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import ca.lrc.beans.Log;
import ca.lrc.beans.Project;
import ca.lrc.beans.Report;
import ca.lrc.beans.Result;

import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.model.testsuite.TestCase;
import com.eviware.soapui.model.testsuite.TestCaseRunner;
import com.eviware.soapui.model.testsuite.TestRunner.Status;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.support.SoapUIException;

/**
 * The ReportController program stores the results (each one instantiated as a
 * new Result POJO) of all the test cases run within a SoapUI project.
 * 
 * @author Daniel Vu
 */
@Controller
@SessionAttributes({ "report", "directoryListing" })
public class HomeController {
	@Value("${path}")
	private String path;

	/**
	 * The Spring Framework refers to the pieces of data accessible during the
	 * execution of views as model attributes. In this method, a new Project
	 * object is instantiated and added as a model attribute so that the user's
	 * environment selection can be persisted to it.
	 * 
	 * @param model
	 *            Allows for accessing the overall model as a java.util.Map.
	 * @return /../WebContent/WEB-INF/jsps/display-report.jsp
	 */
	@RequestMapping(value = "/")
	public String showDisplayReport(Model model) {
		if (!model.containsAttribute("project")) {
			Project project = new Project();
			model.addAttribute("project", project);
		}
		model.addAttribute("directoryListing", getDirectoryListing());
		return "display-report";
	}

	/**
	 * Calls the runSoapUIProject and adds the return value of the call as a
	 * model attribute
	 * 
	 * @param model
	 *            Allows for accessing the overall model as a java.util.Map.
	 * @param project
	 * @return /../WebContent/WEB-INF/jsps/display-report.jsp
	 * @throws SoapUIException
	 * @throws IOException
	 * @throws XmlException
	 */
	@RequestMapping(value = "process-project-selection", method = RequestMethod.POST)
	public String processProjectSelection(Model model,
			@ModelAttribute Project project) throws XmlException, IOException,
			SoapUIException {
		String userChoice = project.getSelection();
		Report report = runSoapUIProject(new WsdlProject(userChoice));
		model.addAttribute("report", report);
		model.addAttribute("directoryListing", getDirectoryListing());
		return "display-report";
	}

	/**
	 * Passes a File object (pointing to a directory) and an empty array into
	 * the createDirectoryListing method.
	 * 
	 * @return the List<File> object from the createDirectoryListing method
	 *         regardless of whether any files were found by the
	 *         createDirectoryListing method and added to aforementioned array.
	 */
	protected List<File> getDirectoryListing() {
		File directory = new File(path);
		List<File> directoryListing = new ArrayList<File>();
		return (createDirectoryListing(directory, directoryListing));
	}

	/**
	 * This method first initializes and instantiates a new Report object. It
	 * then iterates through every test case of every test suite in a SoapUI
	 * project. In each iteration, the result of each test case run (a test case
	 * is run each time by directly calling the runSoapUITestCase method) is
	 * persisted to a new Result object and then added to the aforementioned
	 * Report object's singular ArrayList attribute.
	 * 
	 * @param project
	 *            A representation of the XML file that stores a SoapUI
	 *            project's data and metadata
	 * @return the Report object populated by way of this method's execution
	 */
	protected Report runSoapUIProject(WsdlProject project) {
		Report report = new Report();
		report.setName(project.getName());
		for (int testSuiteIndex = 0; testSuiteIndex < project
				.getTestSuiteList().size(); testSuiteIndex++) {
			for (int testCaseIndex = 0; testCaseIndex < project
					.getTestSuiteAt(testSuiteIndex).getTestCaseList().size(); testCaseIndex++) {
				Result result = new Result();
				result.setSuccessFlag(true);
				result.setName(project.getTestSuiteAt(testSuiteIndex)
						.getTestCaseAt(testCaseIndex).getName());
				TestCase testCase = project.getTestSuiteAt(testSuiteIndex)
						.getTestCaseAt(testCaseIndex);
				runSoapUITestCase(testCase, result);
				report.getResultList().add(result);
			}
		}
		return report;
	}

	/**
	 * This method runs a singular SoapUI test case. It will also modify a
	 * Result object accordingly if any test step within a test case is deemed
	 * by SoapUI to be unsuccessful; the modification consists of setting an
	 * existing Result's success flag from true to false (if not set as such
	 * already) and writing the error log provided by SoapUI to a Log object
	 * embedded within the Result one.
	 * 
	 * @param testCase
	 *            A collection of test steps assembled to test some specific
	 *            aspect of a web service. For our service uptime checker, only
	 *            one test step (a read-only/retrieval operation) currently
	 *            exists in each test case since we only need to test two
	 *            assertions: 1) whether a valid SOAP response was returned and
	 *            2) whether the SOAP response contains a SOAP fault if the
	 *            first assertion was met.
	 * @param result
	 *            A Result object should be initialized and instantiated before
	 *            being passed as a parameter into this method.
	 */
	protected void runSoapUITestCase(TestCase testCase, Result result) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		TestCaseRunner testCaseRunner = testCase.run(null, false);
		for (TestStepResult tcr : testCaseRunner.getResults()) {
			if (testCaseRunner.getStatus() == Status.FAILED) {
				result.setSuccessFlag(false);
				tcr.writeTo(printWriter);
				result.setLog(new Log(stringWriter.toString()));
			}
		}
		printWriter.close();
		try {
			stringWriter.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Adds a Result object's embedded Log object as a model attribute. This
	 * will effectively be parsed to output plain-text in the returned view.
	 * 
	 * @param model
	 *            Allows for accessing the overall model as a java.util.Map.
	 * @param report
	 *            The report selected by the user for server-side processing
	 *            (This program is built to support the eventual archiving of
	 *            reports, and so a specific one must be passed into this method
	 *            each time)
	 * @param index
	 *            Indicates which Result object in a Report object's resultList
	 *            attribute to work with
	 * @return /../WebContent/WEB-INF/jsps/display-log.jsp
	 */
	@RequestMapping(value = "log/{index}")
	public String showLog(Model model, @ModelAttribute Report report,
			@PathVariable int index) {
		String log = report.getResultList().get(index).getLog().getContent();
		model.addAttribute("log", log);
		return "display-log";
	}

	/**
	 * Recursively retrieves the names of all XML files within a directory,
	 * including sub-directories.
	 * 
	 * @param directory
	 *            A location where files are kept in a list. A directory is
	 *            treated as a File object in accordance with the
	 *            oversimplification that everything is a file in the context of
	 *            Unix systems.
	 * @param directoryListing
	 *            An array that all found files get added to by this method.
	 * @return the directoryListing parameter populated by way of this method's
	 *         execution.
	 */
	protected List<File> createDirectoryListing(final File directory,
			List<File> directoryListing) {
		for (final File file : directory.listFiles()) {
			if (file.isDirectory()) {
				createDirectoryListing(file, directoryListing);
			} else if (file.getName().endsWith(".xml")) {
				directoryListing.add(file);
			}
		}
		return directoryListing;
	}
}
